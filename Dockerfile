ARG PYTHON_DOCKER_TAG
FROM python:${PYTHON_DOCKER_TAG}

ENV PYTHONDONTWRITEBYTECODE=1 \
    PYTHONFAULTHANDLER=1 \
    PYTHONUNBUFFERED=1 \
    PYTHONHASHSEED=random \
    PIP_NO_CACHE_DIR=off \
    PIP_DISABLE_PIP_VERSION_CHECK=on \
    PIP_DEFAULT_TIMEOUT=100 \
    POETRY_VIRTUALENVS_CREATE=false \
    POETRY_CACHE_DIR='/var/cache/pypoetry'

RUN pip3 --no-cache-dir install poetry

RUN set -ex && mkdir /app

WORKDIR /app

ONBUILD COPY pyproject.toml pyproject.toml
ONBUILD COPY poetry.lock poetry.lock

ONBUILD RUN set -ex && \
    poetry install --no-dev --no-interaction --no-root && \
    rm -rf "${POETRY_CACHE_DIR}"
